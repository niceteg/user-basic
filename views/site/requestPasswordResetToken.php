<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = 'Recuperar sua senha';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-6 col-lg-offset-3">
	<div class="panel panel-warning">
		<div class="panel-heading">
			<h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-12">
	    		<p>Por favor, informe o seu e-mail. Sua recuperação será enviado para o seu e-mail.</p>
	            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
	            <?= $form->field($model, 'email') ?>
	                <div class="form-group text-center">
	                    <?= Html::submitButton('Recuperar', ['class' => 'btn btn-warning']) ?>
	                </div>
	            <?php ActiveForm::end(); ?>
	        	</div>
    		</div>
		</div>
	</div>
</div>
